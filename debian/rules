#!/usr/bin/make -f
# debian/rules for gitolite package
# Copyright 2010-2011 by Gerfried Fuchs <rhonda@debian.org>
# Licenced under WTFPLv2

PKG = gitolite3
TMP = $(CURDIR)/debian/$(PKG)

INSTALL = install
INSTALL_FILE    = $(INSTALL) -p    -m644
INSTALL_PROGRAM = $(INSTALL) -p    -m755
INSTALL_SCRIPT  = $(INSTALL) -p    -m755
INSTALL_DIR     = $(INSTALL) -p -d -m755

GL_VERSION = $(shell dpkg-parsechangelog | sed -n -e 's/^Version: \(.*\)/\1 (Debian)/p')

clean:
	$(checkdir)

	-rm -rf $(TMP) debian/files

build: build-arch build-indep
build-arch:
build-indep:
	# uhm, build for a binary-indep package?  Don't try to be funny ;)

debian/gitolite.1: debian/gitolite-man.pod
	pod2man --center='User Commands' --release="$(GL_VERSION)" $< > $@

install: debian/gitolite.1
	$(checkdir)

	-rm -rf $(TMP)
	$(INSTALL_DIR) $(TMP)

	cd $(TMP) && $(INSTALL_DIR) usr/share/$(PKG) \
		etc/$(PKG) \
		usr/bin \
		usr/share/man/man1 \
		$(TMP)/usr/share/doc/$(PKG)

	for subdir in lib syntactic-sugar; do \
	   cp -a src/$${subdir} $(TMP)/usr/share/$(PKG); \
	   find $(TMP)/usr/share/$(PKG)/$${subdir} -type f -exec chmod -x {} \; ; \
        done

	for subdir in commands triggers VREF; do \
	   cp -a src/$${subdir} $(TMP)/usr/share/$(PKG); \
	   find $(TMP)/usr/share/$(PKG)/$${subdir} -type f -exec chmod +x {} \; ; \
        done

	$(INSTALL_SCRIPT) src/gitolite $(TMP)/usr/bin
	$(INSTALL_SCRIPT) src/gitolite-shell $(TMP)/usr/share/$(PKG)
	$(INSTALL_SCRIPT) check-g2-compat $(TMP)/usr/share/$(PKG)
	$(INSTALL_SCRIPT) convert-gitosis-conf $(TMP)/usr/share/$(PKG)

	$(INSTALL_FILE) debian/gitolite.1 $(TMP)/usr/share/man/man1
	gzip -9n $(TMP)/usr/share/man/man1/gitolite.1

	printf "%s\n" "$(GL_VERSION)" > $(TMP)/usr/share/$(PKG)/VERSION

	$(INSTALL_FILE) README.markdown $(TMP)/usr/share/doc/$(PKG)

	dh_installdocs
	dh_installchangelogs
	dh_installdebconf

binary-indep: install
	dh_testdir
	dh_testroot
	dh_installdeb
	dh_compress
	dh_gencontrol
	dh_md5sums
	dh_fixperms
	dh_builddeb

binary-arch:
	# We have nothing to do here.


binary: binary-indep


define checkdir
	test -f debian/rules
endef

.PHONY: build clean binary-indep binary-arch binary install
