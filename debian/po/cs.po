# Czech translation of PO debconf template for package gitolite.
# Copyright (C) 2010 Michal Simunek
# This file is distributed under the same license as the gitolite package.
# Michal Simunek <michal.simunek@gmail.com>, 2010 - 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gitolite 3.6-2\n"
"Report-Msgid-Bugs-To: gitolite3@packages.debian.org\n"
"POT-Creation-Date: 2013-05-19 17:14-0300\n"
"PO-Revision-Date: 2014-11-06 08:52+0100\n"
"Last-Translator: Michal Simunek <michal.simunek@gmail.com>\n"
"Language-Team: Czech <debian-l10n-czech@lists.debian.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:2001
msgid "System username for gitolite:"
msgstr "Systémový uživatel pro gitolite:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"Please enter the name for the system user which should be used by gitolite "
"to access repositories. It will be created if necessary."
msgstr ""
"Zadejte prosím jméno systémového uživatele, který má používat gitolite. V "
"případě potřeby bude vytvořen."

#. Type: string
#. Description
#: ../templates:3001
msgid "Repository path:"
msgstr "Cesta k repositářům:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Please enter the path in which gitolite should store the repositories. This "
"will become the gitolite system user's home directory."
msgstr ""
"Zadejte prosím cestu, kam se mají ukládat repositáře gitolite. Nastaví se "
"jako domovský adresář systémového uživatele."

#. Type: string
#. Description
#: ../templates:4001
msgid "Administrator's SSH key:"
msgstr "SSH klíč správce:"

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"Please specify the key of the user that will administer the access "
"configuration of gitolite."
msgstr ""
"Zadejte prosím klíč uživatele, který bude spravovat nastavení přístupu ke "
"gitolite."

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"This can be either the SSH public key itself, or the path to a file "
"containing it. If it is blank, gitolite will be left unconfigured and must "
"be set up manually."
msgstr ""
"Může to být jak samotný veřejný klíč, tak cesta k souboru, který jej "
"obsahuje. Ponecháte-li jej prázdný, bude gitolite ponechán nenastavený a "
"bude muset být nastaven ručně."

#. Type: string
#. Description
#: ../templates:4001
msgid "If migrating from gitolite version 2.x, leave this blank."
msgstr "Pokud přecházíte z gitolite verze 2.x, ponechte políčko prázdné."
